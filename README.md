# FileExecutor

A tiny plugin that can be used to execute commands in a file from console. This is handy for executing commands in bulk after compiling a list of commands with a script or your favourite text editor.

## Installation

Drop the plugin into your Bukkit plugins folder. During startup an example file will be placed under FileExecutor in your plugins folder. Put your text files with commands into that same folder.

## Commands

This section describes all commands this plugin provides.

| Command | Description
| ------- | -----------
| /fileexecute \<file\> | Execute the commands in a file

## Permissions

This plugin has no permissions as the main command can only be executed via the console.
