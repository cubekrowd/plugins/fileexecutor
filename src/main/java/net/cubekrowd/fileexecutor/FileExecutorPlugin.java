package net.cubekrowd.fileexecutor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;
import java.util.StringJoiner;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class FileExecutorPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        if (!(new File(getDataFolder(), "sample.txt").exists())) {
            saveResource("sample.txt", false);
        }
    }

    public void executeLineAndScheduleNext(CommandSender sender, List<String> lines, int index) {
        for (;;) {
            if (lines.size() <= index) {
                sender.sendMessage("All commands have been executed");
                return;
            }

            var line = lines.get(index);
            index++;

            if (line.isBlank() || line.startsWith("#")) {
                continue;
            }

            sender.sendMessage("Executing: " + line);
            try {
                Bukkit.dispatchCommand(sender, line);
            } catch (CommandException e) {
                getLogger().log(Level.WARNING, "Failed to execute command", e);
                sender.sendMessage("Something went wrong while executing the command");
                return;
            }

            int nextIndex = index;
            Bukkit.getScheduler().runTaskLater(this, () -> {
                executeLineAndScheduleNext(sender, lines, nextIndex);
            }, 2);
            break;
        }
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof ConsoleCommandSender)) {
            sender.sendMessage("This command can only be executed from the console.");
            return true;
        }

        if (args.length == 0) {
            return false;
        }

        var desiredFile = args[0].toLowerCase(Locale.ENGLISH);
        List<Path> fileList = null;

        try {
            fileList = Files.list(getDataFolder().toPath()).toList();
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Failed to list files", e);
            sender.sendMessage("Something went wrong. Try again later.");
            return true;
        }

        Path foundFile = null;

        for (var file : fileList) {
            var fileName = file.getFileName().toString().toLowerCase(Locale.ENGLISH);
            if (fileName.equals(desiredFile) || fileName.startsWith(desiredFile + ".")) {
                foundFile = file;
                break;
            }
        }

        if (foundFile == null) {
            var joiner = new StringJoiner(", ");
            joiner.setEmptyValue("none");
            for (var file : fileList) {
                joiner.add(file.getFileName().toString());
            }
            sender.sendMessage(String.format("No matching files found. Available files: %s", joiner));
            return true;
        }

        sender.sendMessage(String.format("Executing all commands in %s", foundFile.getFileName()));

        List<String> lines = null;

        try {
            lines = Files.readAllLines(foundFile);
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Failed to read file", e);
            sender.sendMessage("Something went wrong. Try again later.");
            return true;
        }

        executeLineAndScheduleNext(sender, lines, 0);

        return true;
    }
}
